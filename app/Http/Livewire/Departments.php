<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Departments extends Component
{
	public $name;
	
	public function modelFlagOpen(){
		$this->emit('openModel', ['flag'=>'show']);
	}
	
	public function modelFlagClose(){
		
		$this->emit('openModel', ['flag'=>'hide']);
	}
	public function updated($field)
    {
        $this->validateOnly($field, [
            'name' => 'required|min:2',
        ]);
    }

    public function addDepartment(){
    	 $this->validate([
            'name' => 'required|min:2',           
        ]);
    	
	    Department::create([
            'name' => $this->name,
        ]);
    }
    public function render()
    {
        return view('livewire.departments');
    }
}
