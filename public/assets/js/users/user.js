/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/users/user.js":
/*!*******************************************!*\
  !*** ./resources/assets/js/users/user.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(function () {
  $('#projectId,#editProjectId').select2({
    width: '100%'
  });
  $('#roleId,#editRoleId').select2({
    width: '100%',
    placeholder: 'Select Role',
    minimumResultsForSearch: -1
  });
});
$(document).ready(function () {
  $('input').attr('autocomplete', 'false');
});
var tbl = $('#users_table').DataTable({
  processing: true,
  serverSide: true,
  'order': [[0, 'asc']],
  ajax: {
    url: usersUrl
  },
  columnDefs: [{
    'targets': [6],
    'orderable': false,
    'className': 'text-center',
    'width': '5%'
  }, {
    'targets': [5],
    'orderable': false,
    'className': 'text-center',
    'width': '5%'
  }, {
    'targets': [4],
    'className': 'text-center',
    'width': '4%'
  }, {
    'targets': [3, 7],
    'orderable': false,
    'className': 'text-center',
    'width': '6%'
  }],
  columns: [{
    data: 'name',
    name: 'name'
  }, {
    data: 'email',
    name: 'email'
  }, {
    data: 'phone',
    name: 'phone'
  }, {
    data: 'role_name',
    name: 'role_name',
    'searchable': false
  }, {
    data: 'salary',
    name: 'salary'
  }, {
    data: function data(row) {
      var checked = row.is_active === 0 ? '' : 'checked';

      if (loggedInUserId === row.id) {
        return '';
      }

      return ' <label class="switch switch-label switch-outline-primary-alt">' + '<input name="is_active" data-id="' + row.id + '" class="switch-input is-active" type="checkbox" value="1" ' + checked + '>' + '<span class="switch-slider" data-checked="&#x2713;" data-unchecked="&#x2715;"></span>' + '</label>';
    },
    name: 'id'
  }, {
    data: function data(row) {
      var email_verification = '<button type="button" title="Send Verification Mail" id="email-btn" class="btn action-btn btn-primary btn-sm email-btn" ' + 'data-loading-text="<span class=\'spinner-border spinner-border-sm\'></span>" data-id="' + row.id + '">' + '<i class="icon-envelope icons action-icon"></i></button>';

      if (row.is_email_verified) {
        email_verification = '<a title="Email Verified" data-id="' + row.id + '">' + '<i class="cui-circle-check check-icon"></i></a>';
      }

      return email_verification;
    },
    name: 'id'
  }, {
    data: function data(row) {
      return '<a title="Edit" class="btn action-btn btn-primary btn-sm edit-btn mr-1" data-id="' + row.id + '">' + '<i class="cui-pencil action-icon"  style="color:#3c8dbc"></i>' + '</a>' + '<a title="Delete" class="btn action-btn btn-danger btn-sm delete-btn" data-id="' + row.id + '">' + '<i class="cui-trash action-icon text-danger"></i></a>';
    },
    name: 'id'
  }]
});
$('#users_table').on('draw.dt', function () {
  $('[data-toggle="tooltip"]').tooltip();
});

window.renderData = function (url) {
  $.ajax({
    url: url,
    type: 'GET',
    success: function success(result) {
      if (result.success) {
        var user = result.data;
        $('#userId').val(user.id);
        $('#edit_name').val(user.name);
        $('#edit_email').val(user.email);
        $('#edit_phone').val(user.phone);
        $('#edit_salary').val(user.salary);
        $('#editProjectId').val(user.project_ids).trigger('change');

        if (user.is_active) {
          $('#edit_is_active').val(1).prop('checked', true);
        }

        $('#editRoleId').val(user.role_id).trigger('change');
        $('#EditModal').modal('show');
      }
    },
    error: function error(_error) {
      manageAjaxErrors(_error);
    }
  });
};

window.sendVerificationEmail = function (url) {
  var loadingButton = $('#email-btn');
  loadingButton.button('loading');
  $.ajax({
    url: url,
    type: 'GET',
    success: function success(result) {
      if (result.success) {
        swal('Success!', result.message, 'success');
      }
    },
    error: function error(_error2) {
      manageAjaxErrors(_error2);
    },
    complete: function complete() {
      loadingButton.button('reset');
    }
  });
};

$(function () {
  // create new user
  $('#addNewForm').submit(function (event) {
    event.preventDefault();
    var loadingButton = jQuery(this).find('#btnSave');
    loadingButton.button('loading');
    $.ajax({
      url: createUserUrl,
      type: 'POST',
      data: $(this).serialize(),
      success: function success(result) {
        if (result.success) {
          displaySuccessMessage(result.message);
          $('#AddModal').modal('hide');
          $('#users_table').DataTable().ajax.reload(null, false);
        }
      },
      error: function error(result) {
        printErrorMessage('#validationErrorsBox', result);
      },
      complete: function complete() {
        loadingButton.button('reset');
      }
    });
  }); // update user

  $('#editForm').submit(function (event) {
    event.preventDefault();
    var loadingButton = jQuery(this).find('#btnEditSave');
    loadingButton.button('loading');
    var id = $('#userId').val();
    $.ajax({
      url: usersUrl + id,
      type: 'put',
      data: $(this).serialize(),
      success: function success(result) {
        if (result.success) {
          displaySuccessMessage(result.message);
          $('#EditModal').modal('hide');
          $('#users_table').DataTable().ajax.reload(null, false);
        }
      },
      error: function error(_error3) {
        manageAjaxErrors(_error3);
      },
      complete: function complete() {
        loadingButton.button('reset');
      }
    });
  });
  $('#AddModal').on('hidden.bs.modal', function () {
    $('#projectId').val(null).trigger('change');
    resetModalForm('#addNewForm', '#validationErrorsBox');
  });
  $('#EditModal').on('hidden.bs.modal', function () {
    resetModalForm('#editForm', '#editValidationErrorsBox');
  }); // open edit user model

  $(document).on('click', '.edit-btn', function (event) {
    var userId = $(event.currentTarget).data('id');
    renderData(usersUrl + userId + '/edit');
  }); // open delete confirmation model

  $(document).on('click', '.delete-btn', function (event) {
    var userId = $(event.currentTarget).data('id');
    deleteItem(usersUrl + userId, '#users_table', 'User');
  });
  $(document).on('click', '.email-btn', function (event) {
    var userId = $(event.currentTarget).data('id');
    sendVerificationEmail(usersUrl + userId + '/send-email');
  });
}); // listen user activation deactivation change event

$(document).on('change', '.is-active', function (event) {
  var userId = $(event.currentTarget).data('id');
  activeDeActiveUser(userId);
}); // activate de-activate user

window.activeDeActiveUser = function (id) {
  $.ajax({
    url: usersUrl + id + '/active-de-active',
    method: 'post',
    cache: false,
    success: function success(result) {
      if (result.success) {
        tbl.ajax.reload();
      }
    }
  });
};

/***/ }),

/***/ 2:
/*!*************************************************!*\
  !*** multi ./resources/assets/js/users/user.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/infy-tracker/resources/assets/js/users/user.js */"./resources/assets/js/users/user.js");


/***/ })

/******/ });