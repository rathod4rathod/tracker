/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 9);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/report/report.js":
/*!**********************************************!*\
  !*** ./resources/assets/js/report/report.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var departmentDropDown = $('#department');
$('#clientId').select2({
  width: '100%',
  placeholder: 'Select Client'
}).prepend($('<option>', {
  value: 0,
  text: 'None'
}));
departmentDropDown.select2({
  width: '100%',
  placeholder: 'Select Department'
}).prepend($('<option>', {
  value: 0,
  text: 'None'
}));
$('#projectIds').select2({
  width: '100%',
  placeholder: 'Select Projects'
});
$('#userIds').select2({
  width: '100%',
  placeholder: 'Select Users'
});
$('#tagIds').select2({
  width: '100%',
  placeholder: 'Select Tags'
});
$('#filterCreatedBy').select2();
$('.select2-search__field').css('width', '100%');
$('#start_date').datetimepicker({
  format: 'YYYY-MM-DD',
  useCurrent: true,
  icons: {
    previous: 'icon-arrow-left icons',
    next: 'icon-arrow-right icons'
  },
  sideBySide: true,
  maxDate: moment()
});
$('#end_date').datetimepicker({
  format: 'YYYY-MM-DD',
  useCurrent: false,
  icons: {
    previous: 'icon-arrow-left icons',
    next: 'icon-arrow-right icons'
  },
  sideBySide: true,
  maxDate: moment()
});
$(function () {
  $('form').find('input:text').filter(':input:visible:first').first().focus();
});
$('#start_date').on('dp.change', function (e) {
  $('#end_date').data('DateTimePicker').minDate(e.date);
});
departmentDropDown.on('change', function () {
  if (+$(this).val() !== 0) {
    $('#clientId').val(null).trigger('change');
  }

  $('#clientId').html('<option value="0">None</option>');
  loadClient(parseInt($(this).val()));
});

function loadClient(departmentId) {
  departmentId = departmentId === 0 ? '' : departmentId;
  var url = clientsOfDepartment + '?department_id=' + departmentId;
  $.ajax({
    url: url,
    type: 'GET',
    success: function success(result) {
      var clients = result.data;
      var options = '<option value="0">None</option>';
      $.each(clients, function (key, value) {
        options += '<option value="' + key + '">' + value + '</option>';
      });
      $('#clientId').html(options);
    }
  });
}

$('#clientId').on('change', function () {
  $('#projectIds').empty();

  if ($(this).val() != 0) {
    $('#projectIds').val(null).trigger('change');
  }

  loadProjects($(this).val());
});

function loadProjects(clientId) {
  clientId = clientId == 0 ? '' : clientId;
  var url = projectsOfClient + '?client_id=' + clientId;
  $.ajax({
    url: url,
    type: 'GET',
    success: function success(result) {
      var projects = result.data;

      for (var key in projects) {
        if (projects.hasOwnProperty(key)) {
          $('#projectIds').append($('<option>', {
            value: key,
            text: projects[key]
          }));
        }
      }
    }
  });
}

$('#projectIds').on('change', function () {
  $('#userIds').empty();
  $('#userIds').val(null).trigger('change');
  loadUsers($(this).val().toString());
});

function loadUsers(projectIds) {
  var url = usersOfProjects + '?projectIds=' + projectIds;
  $.ajax({
    url: url,
    type: 'GET',
    success: function success(result) {
      var users = result.data;

      for (var key in users) {
        if (users.hasOwnProperty(key)) {
          $('#userIds').append($('<option>', {
            value: key,
            text: users[key]
          }));
        }
      }
    }
  });
} // open delete confirmation model


$(document).on('click', '.delete-btn', function (event) {
  var reportId = $(event.currentTarget).data('id');
  deleteReport(reportUrl + reportId);
});

window.deleteReport = function (url) {
  swal({
    title: 'Delete !',
    text: 'Are you sure you want to delete this "Report" ?',
    type: 'warning',
    showCancelButton: true,
    closeOnConfirm: false,
    showLoaderOnConfirm: true,
    confirmButtonColor: '#5cb85c',
    cancelButtonColor: '#d33',
    cancelButtonText: 'No',
    confirmButtonText: 'Yes'
  }, function () {
    $.ajax({
      url: url,
      type: 'DELETE',
      dataType: 'json',
      success: function success(obj) {
        if (obj.success) {
          location.href = reportUrl;
        }

        swal({
          title: 'Deleted!',
          text: 'Report has been deleted.',
          type: 'success',
          timer: 2000
        });
      },
      error: function error(data) {
        swal({
          title: '',
          text: data.responseJSON.message,
          type: 'error',
          timer: 5000
        });
      }
    });
  });
};

var tbl = $('#report_table').DataTable({
  processing: true,
  serverSide: true,
  'order': [[0, 'asc']],
  ajax: {
    url: reportUrl,
    data: function data(_data) {
      _data.filter_created_by = $('#filterCreatedBy').find('option:selected').val();
    }
  },
  columnDefs: [{
    'targets': [1, 2, 3],
    'width': '10%',
    'className': 'text-center'
  }, {
    'targets': [4],
    'orderable': false,
    'className': 'text-center',
    'width': '8%'
  }],
  columns: [{
    data: 'name',
    name: 'name'
  }, {
    data: function data(row) {
      return format(row.start_date, 'YYYY-MMM-DD');
    },
    name: 'start_date'
  }, {
    data: function data(row) {
      return format(row.end_date, 'YYYY-MMM-DD');
    },
    name: 'end_date'
  }, {
    data: 'user.name',
    defaultContent: '',
    name: 'user.name'
  }, {
    data: function data(row) {
      return '<a title="Run Report" class="btn action-btn btn-success btn-sm mr-1" href="' + reportUrl + row.id + '">' + '<i class="fas fa-eye action-icon"></i>' + '</a>' + '<a title="Edit" class="btn action-btn btn-primary btn-sm edit-btn mr-1" href="' + reportUrl + row.id + '/edit">' + '<i class="cui-pencil action-icon"></i>' + '</a>' + '<a title="Delete" class="btn action-btn btn-danger btn-sm delete-btn" data-id="' + row.id + '">' + '<i class="cui-trash action-icon" ></i></a>';
    },
    name: 'id'
  }],
  'fnInitComplete': function fnInitComplete() {
    $('#filterClient,#filterCreatedBy').change(function () {
      tbl.ajax.reload();
    });
  }
});

/***/ }),

/***/ 9:
/*!****************************************************!*\
  !*** multi ./resources/assets/js/report/report.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/infy-tracker/resources/assets/js/report/report.js */"./resources/assets/js/report/report.js");


/***/ })

/******/ });