/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/time_entries/time_entry.js":
/*!********************************************************!*\
  !*** ./resources/assets/js/time_entries/time_entry.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$('#taskId,#editTaskId').select2({
  width: '100%',
  placeholder: 'Select Task'
});
$('#duration').prop('disabled', true);
$('#timeProjectId,#editTimeProjectId').select2({
  width: '100%',
  placeholder: 'Select Project'
});
$('#filterActivity,#filterUser,#filter_project').select2();
$('#activityTypeId,#editActivityTypeId').select2({
  width: '100%',
  placeholder: 'Select Activity Type'
});
var isEdit = false;
var editTaskId,
    editProjectId = null;
var tbl = $('#timeEntryTable').DataTable({
  processing: true,
  serverSide: true,
  'order': [[9, 'desc']],
  ajax: {
    url: timeEntryUrl,
    data: function data(_data) {
      _data.filter_project = $('#filter_project').find('option:selected').val();
      _data.filter_activity = $('#filterActivity').find('option:selected').val();
      _data.filter_user = $('#filterUser').find('option:selected').val();
    }
  },
  columnDefs: [{
    'targets': [9],
    'width': '7%',
    'className': 'text-center',
    'visible': false
  }, {
    'targets': [6],
    'width': '9%'
  }, {
    'targets': [7],
    'width': '4%'
  }, {
    'targets': [4, 5],
    'width': '10%'
  }, {
    'targets': [8],
    'orderable': false,
    'className': 'text-center',
    'width': '5%'
  }, {
    'targets': [3],
    'width': '8%'
  }, {
    'targets': [0, 1],
    'width': '3%'
  }],
  columns: [{
    className: 'details-control',
    defaultContent: '<a class=\'btn btn-success collapse-icon action-btn btn-sm\'><span class=\'fa fa-plus-circle action-icon\'></span></a>',
    data: null,
    orderable: false,
    searchable: false
  }, {
    data: function data(row) {
      if (row.user) {
        return '<img class="assignee__avatar" src="' + row.user.img_avatar + '" data-toggle="tooltip" title="' + row.user.name + '">';
      } else {
        return '';
      }
    },
    name: 'user.name'
  }, {
    data: function data(row) {
      var taskPrefix = row.task.project.prefix + '-' + row.task.task_number;
      var url = taskUrl + taskPrefix;
      return '<a href="' + url + '">' + row.title + '</a>';
    },
    name: 'title'
  }, {
    data: 'activity_type.name',
    name: 'activityType.name',
    defaultContent: ''
  }, {
    data: 'start_time',
    name: 'start_time'
  }, {
    data: 'end_time',
    name: 'end_time'
  }, {
    data: function data(row) {
      return roundToQuarterHourAll(row.duration);
    },
    name: 'duration'
  }, {
    data: function data(row) {
      return row;
    },
    render: function render(row) {
      if (row.entry_type == 1) {
        return '<span class="badge badge-primary">' + row.entry_type_string + '</span>';
      }

      return '<span class="badge badge-secondary">' + row.entry_type_string + '</span>';
    },
    name: 'entry_type'
  }, {
    data: function data(row) {
      return '<a title="Edit" class="btn action-btn btn-primary btn-sm btn-edit mr-1" data-id="' + row.id + '">' + '<i class="cui-pencil action-icon"></i>' + '</a>' + '<a title="Delete" class="btn action-btn btn-danger btn-sm btn-delete" data-id="' + row.id + '" >' + '<i class="cui-trash action-icon"></i></a>';
    },
    name: 'id'
  }, {
    data: function data(row) {
      return row;
    },
    render: function render(row) {
      return '<span data-toggle="tooltip" title="' + format(row.created_at, 'hh:mm:ss a') + '">' + format(row.created_at) + '</span>';
    },
    name: 'created_at'
  }],
  'fnInitComplete': function fnInitComplete() {
    $('#filterActivity,#filterUser,#filterTask,#filter_project').change(function () {
      tbl.ajax.reload();
    });
  }
});
$('#timeEntryTable tbody').off('click', 'tr td.details-control');
$('#timeEntryTable tbody').on('click', 'tr td.details-control', function () {
  var tr = $(this).closest('tr');
  var row = tbl.row(tr);

  if (row.child.isShown()) {
    $(this).children().children().removeClass('fa-minus-circle').addClass('fa-plus-circle');
    row.child.hide();
    tr.removeClass('shown');
  } else {
    $(this).children().children().removeClass('fa-plus-circle').addClass('fa-minus-circle');
    row.child('<div style="padding-left:50px;">' + nl2br(row.data().note) + '</div>').show();
    tr.addClass('shown');
  }
});

if (!canManageEntries) {
  tbl.columns([1]).visible(false);
}

$('#timeEntryTable').on('draw.dt', function () {
  $('[data-toggle="tooltip"]').tooltip();
});
$('#timeEntryAddForm').submit(function (event) {
  event.preventDefault();
  $('#taskId').removeAttr('disabled');
  var loadingButton = jQuery(this).find('#btnSave');
  loadingButton.button('loading');
  $.ajax({
    url: storeTimeEntriesUrl,
    type: 'POST',
    data: $(this).serialize(),
    success: function success(result) {
      if (result.success) {
        $('#timeEntryAddModal').modal('hide');
        $('#timeEntryTable').DataTable().ajax.reload(null, false);
      }
    },
    error: function error(result) {
      printErrorMessage('#tmValidationErrorsBox', result);
    },
    complete: function complete() {
      loadingButton.button('reset');
    }
  });
});
$('#timeEntryAddModal').on('hidden.bs.modal', function () {
  isEdit = false;
  $('#startTime').data('DateTimePicker').date(null);
  $('#endTime').data('DateTimePicker').date(null);
  $('#taskId').val(null).trigger('change');
  $('#activityTypeId').val(null).trigger('change');
  $('#duration').prop('disabled', false);
  $('#startTime').prop('disabled', false);
  $('#endTime').prop('disabled', false);
  resetModalForm('#timeEntryAddForm', '#tmValidationErrorsBox');
});
$('#startTime,#endTime').on('dp.change', function () {
  var startTime = $('#startTime').val();
  var endTime = $('#endTime').val();
  var minutes = 0;

  if (endTime) {
    var diff = new Date(Date.parse(endTime) - Date.parse(startTime));
    minutes = diff / (1000 * 60);

    if (!Number.isInteger(minutes)) {
      minutes = minutes.toFixed(2);
    }
  }

  $('#duration').val(minutes).prop('disabled', true);
});
$('#startTime').attr('placeholder', 'YYYY-MM-DD HH:mm:ss');
$('#endTime').attr('placeholder', 'YYYY-MM-DD HH:mm:ss');
$('#dvStartTime,#dvEndTime').on('click', function () {
  $('#startTime').removeAttr('disabled');
  $('#endTime').removeAttr('disabled');
  $('#duration').prop('disabled', true);
});
$('#editStartTime,#editEndTime').on('dp.change', function () {
  var startTime = $('#editStartTime').val();
  var endTime = $('#editEndTime').val();
  var minutes = 0;

  if (endTime) {
    var diff = new Date(Date.parse(endTime) - Date.parse(startTime));
    minutes = diff / (1000 * 60);

    if (!Number.isInteger(minutes)) {
      minutes = minutes.toFixed(2);
    }
  }

  $('#editDuration').val(minutes).prop('disabled', true);
  $('#editStartTime').data('DateTimePicker').maxDate(moment().endOf('now'));
  $('#editEndTime').data('DateTimePicker').maxDate(moment().endOf('now'));
});
$('#startTime,#editStartTime').datetimepicker({
  format: 'YYYY-MM-DD HH:mm:ss',
  useCurrent: true,
  icons: {
    up: 'icon-arrow-up icons',
    down: 'icon-arrow-down icons',
    previous: 'icon-arrow-left icons',
    next: 'icon-arrow-right icons'
  },
  sideBySide: true,
  maxDate: moment().endOf('day')
});
$('#endTime,#editEndTime').datetimepicker({
  format: 'YYYY-MM-DD HH:mm:ss',
  useCurrent: true,
  icons: {
    up: 'icon-arrow-up icons',
    down: 'icon-arrow-down icons',
    previous: 'icon-arrow-left icons',
    next: 'icon-arrow-right icons'
  },
  sideBySide: true,
  maxDate: moment().endOf('day')
});
$('#startTime,#endTime').on('dp.change', function (selected) {
  $('#startTime').data('DateTimePicker').maxDate(moment().endOf('now'));
  $('#endTime').data('DateTimePicker').maxDate(moment().endOf('now'));
});
$('#editTimeEntryForm').submit(function (event) {
  event.preventDefault();
  var loadingButton = jQuery(this).find('#btnEditSave');
  loadingButton.button('loading');
  var id = $('#entryId').val();
  $.ajax({
    url: timeEntryUrl + id + '/update',
    type: 'post',
    data: $(this).serialize(),
    success: function success(result) {
      if (result.success) {
        $('#editTimeEntryModal').modal('hide');
        $('#timeEntryTable').DataTable().ajax.reload(null, false);

        if ($.isFunction(window.taskDetails)) {
          taskDetails(result.data.task_id);
        }
      }
    },
    error: function error(_error) {
      manageAjaxErrors(_error, 'teEditValidationErrorsBox');
    },
    complete: function complete() {
      loadingButton.button('reset');
    }
  });
});
$('#editTimeEntryModal').on('hidden.bs.modal', function () {
  $('#editDuration').prop('disabled', false);
  $('#editStartTime').prop('disabled', false);
  $('#editEndTime').prop('disabled', false);
  resetModalForm('#editTimeEntryForm', '#teEditValidationErrorsBox');
});

window.renderTimeEntry = function (id) {
  $.ajax({
    url: timeEntryUrl + id + '/edit',
    type: 'GET',
    success: function success(result) {
      if (result.success) {
        var timeEntry = result.data;
        editTaskId = timeEntry.task_id;
        editProjectId = timeEntry.project_id;
        $('#editTimeProjectId').val(timeEntry.project_id).trigger('change');
        $('#entryId').val(timeEntry.id);
        $('#editTaskId').val(timeEntry.task_id).trigger('change');
        $('#editActivityTypeId').val(timeEntry.activity_type_id).trigger('change');
        $('#editDuration').val(timeEntry.duration);
        $('#editStartTime').val(timeEntry.start_time);
        $('#editEndTime').val(timeEntry.end_time);
        $('#editNote').val(timeEntry.note);
        $('#editTimeEntryModal').modal('show'); //add it cause of project_id change, when it change it sets tasks dynamically and selected task_id vanished

        setTimeout(function () {
          $('#editTaskId').val(timeEntry.task_id).trigger('change');
        }, 1500);
      }
    },
    error: function error(_error2) {
      manageAjaxErrors(_error2, 'teEditValidationErrorsBox');
    }
  });
};

$(document).on('click', '.btn-edit', function (event) {
  var timeId = $(event.currentTarget).data('id');
  renderTimeEntry(timeId);
});
$(document).on('click', '.btn-delete', function (event) {
  var timeId = $(event.currentTarget).data('id');
  deleteItem(timeEntryUrl + timeId, '#timeEntryTable', 'Time Entry');
});

window.getTasksByProject = function (projectId, taskId, selectedId, errorBoxId) {
  if (!(projectId > 0)) {
    return false;
  }

  var taskURL = projectsURL + projectId + '/tasks';
  taskURL = isEdit ? taskURL + '?task_id=' + editTaskId : taskURL;
  $.ajax({
    url: taskURL,
    type: 'get',
    success: function success(result) {
      var tasks = result.data;

      if (selectedId > 0) {
        var options = '<option value="0" disabled>Select Task</option>';
      } else {
        var options = '<option value="0" disabled selected>Select Task</option>';
      }

      $.each(tasks, function (key, value) {
        if (selectedId > 0 && selectedId == key) {
          options += '<option value="' + key + '" selected>' + value + '</option>';
        } else {
          options += '<option value="' + key + '">' + value + '</option>';
        }
      });
      $(taskId).html(options);

      if (selectedId > 0) {
        $(taskId).val(selectedId).trigger('change');
      }
    },
    error: function error(result) {
      printErrorMessage(errorBoxId, result);
    }
  });
};

$('#timeProjectId').on('change', function () {
  $('#taskId').select2('val', '');
  var projectId = $(this).val();
  getTasksByProject(projectId, '#taskId', 0, '#tmValidationErrorsBox');
});
$('#editTimeProjectId').on('change', function () {
  $('#editTaskId').select2('val', '');
  var projectId = $(this).val();
  isEdit = editProjectId == projectId ? true : false;
  getTasksByProject(projectId, '#editTaskId', 0, '#teEditValidationErrorsBox');
});
$('#new_entry').click(function () {
  var tracketProjectId = localStorage.getItem('project_id');
  $('#timeProjectId').val(tracketProjectId);
  $('#timeProjectId').trigger('change');
  getTasksByProject(tracketProjectId, '#taskId', 0, '#tmValidationErrorsBox');
  $('#endTime').val(moment().format('YYYY-MM-DD HH:mm:ss'));
});

/***/ }),

/***/ 3:
/*!**************************************************************!*\
  !*** multi ./resources/assets/js/time_entries/time_entry.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/infy-tracker/resources/assets/js/time_entries/time_entry.js */"./resources/assets/js/time_entries/time_entry.js");


/***/ })

/******/ });