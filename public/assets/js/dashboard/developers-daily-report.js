/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 11);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/dashboard/developers-daily-report.js":
/*!******************************************************************!*\
  !*** ./resources/assets/js/dashboard/developers-daily-report.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var $datePicker = $('#developers-report-date-picker');
var start = moment();
$(window).on('load', function () {
  loadDevelopersWorkReport(start.format('YYYY-MM-D  H:mm:ss'));
});
$datePicker.on('apply.daterangepicker', function (ev, picker) {
  var startDate = picker.startDate.format('YYYY-MM-D  H:mm:ss');
  loadDevelopersWorkReport(startDate);
});

window.cb = function (start) {
  $datePicker.find('span').html(start.format('MMM D, YYYY'));
};

cb(start);
$datePicker.daterangepicker({
  startDate: start,
  opens: 'left',
  maxDate: moment(),
  autoUpdateInput: false,
  singleDatePicker: true
}, cb);

window.loadDevelopersWorkReport = function (startDate) {
  $.ajax({
    type: 'GET',
    url: userDeveloperReportUrl,
    dataType: 'json',
    data: {
      start_date: startDate
    },
    cache: false
  }).done(prepareDeveloperWorkReport);
};

window.prepareDeveloperWorkReport = function (result) {
  $('#developers-daily-work-report-container').html('');
  var data = result.data;

  if (data.totalRecords === 0) {
    $('#developers-daily-work-report-container').empty();
    $('#developers-daily-work-report-container').append('<div align="center" class="no-record">No Records Found</div>');
    return true;
  } else {
    $('#developers-daily-work-report-container').html('');
    $('#developers-daily-work-report-container').append('<canvas id="developers-daily-work-report"></canvas>');
  }

  var ctx = document.getElementById('developers-daily-work-report').getContext('2d');
  ctx.canvas.style.height = '500px';
  ctx.canvas.style.width = '100%';
  var dailyWorkReportChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: data.data.labels,
      datasets: [{
        label: data.label,
        data: data.data.data,
        backgroundColor: data.data.backgroundColor,
        borderColor: data.data.borderColor,
        borderWidth: 1
      }]
    },
    options: {
      tooltips: {
        mode: 'index',
        callbacks: {
          label: function label(tooltipItem, data) {
            var label = data.datasets[tooltipItem.datasetIndex].label || '';

            if (label) {
              label += ': ';
            }

            result = convertToTimeFormat(tooltipItem.yLabel);
            return label + result;
          }
        }
      },
      scales: {
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Hours'
          }
        }]
      },
      legend: {
        display: false
      }
    }
  });
};

window.convertToTimeFormat = function (duration) {
  var totalTime = duration.toString().split('.');
  var hours = parseInt(totalTime[0]);
  var minutes = Math.floor(duration * 60) - Math.floor(hours * 60);

  if (hours === 0) {
    return minutes + 'min';
  }

  if (minutes > 0) {
    return hours + 'hr ' + minutes + 'min';
  }

  return hours + 'hr';
};

/***/ }),

/***/ 11:
/*!************************************************************************!*\
  !*** multi ./resources/assets/js/dashboard/developers-daily-report.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/infy-tracker/resources/assets/js/dashboard/developers-daily-report.js */"./resources/assets/js/dashboard/developers-daily-report.js");


/***/ })

/******/ });