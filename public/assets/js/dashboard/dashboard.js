/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 10);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/dashboard/dashboard.js":
/*!****************************************************!*\
  !*** ./resources/assets/js/dashboard/dashboard.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$('#userId').select2({
  width: '110%',
  placeholder: 'Select User'
});
var timeRange = $('#time_range');
var today = moment();
var start = today.clone().startOf('month');
var end = today.clone().endOf('month');
var userId = $('#userId').val();
var isPickerApply = false;
$(window).on('load', function () {
  loadUserWorkReport(start.format('YYYY-MM-D  H:mm:ss'), end.format('YYYY-MM-D  H:mm:ss'), userId);
});
timeRange.on('apply.daterangepicker', function (ev, picker) {
  isPickerApply = true;
  start = picker.startDate.format('YYYY-MM-D  H:mm:ss');
  end = picker.endDate.format('YYYY-MM-D  H:mm:ss');
  loadUserWorkReport(start, end, userId);
});

window.cb = function (start, end) {
  timeRange.find('span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
};

cb(start, end);
var lastMonth = moment().startOf('month').subtract(1, 'days');
timeRange.daterangepicker({
  startDate: start,
  endDate: end,
  opens: 'left',
  showDropdowns: true,
  autoUpdateInput: false,
  ranges: {
    'Today': [moment(), moment()],
    'This Week': [moment().startOf('week'), moment().endOf('week')],
    'Last Week': [moment().startOf('week').subtract(7, 'days'), moment().startOf('week').subtract(1, 'days')],
    'This Month': [start, end],
    'Last Month': [lastMonth.clone().startOf('month'), lastMonth.clone().endOf('month')]
  }
}, cb);
$('#userId').on('change', function (e) {
  e.preventDefault();
  userId = $('#userId').val();
  var startDate = isPickerApply ? start : start.format('YYYY-MM-D  H:mm:ss');
  var endDate = isPickerApply ? end : end.format('YYYY-MM-D  H:mm:ss');
  loadUserWorkReport(startDate, endDate, userId);
});

window.loadUserWorkReport = function (startDate, endDate, userId) {
  $.ajax({
    type: 'GET',
    url: userReportUrl,
    dataType: 'json',
    data: {
      start_date: startDate,
      end_date: endDate,
      user_id: userId
    },
    cache: false
  }).done(prepareUserWorkReport);
};

window.prepareUserWorkReport = function (result) {
  $('#daily-work-report').html('');
  var data = result.data;

  if (data.totalRecords === 0) {
    $('#work-report-container').html('');
    $('#work-report-container').append('<div align="center" class="no-record">No Records Found</div>');
    return true;
  } else {
    $('#work-report-container').html('');
    $('#work-report-container').append('<canvas id="daily-work-report"></canvas>');
  }

  var barChartData = {
    labels: data.date,
    datasets: data.data
  };
  var ctx = document.getElementById('daily-work-report').getContext('2d');
  ctx.canvas.style.height = '400px';
  ctx.canvas.style.width = '100%';
  window.myBar = new Chart(ctx, {
    type: 'bar',
    data: barChartData,
    options: {
      title: {
        display: false,
        text: data.label
      },
      tooltips: {
        mode: 'index',
        callbacks: {
          label: function label(tooltipItem, data) {
            result = roundToQuarterHour(tooltipItem.yLabel);

            if (result == '0min') {
              return '';
            }

            var label = data.datasets[tooltipItem.datasetIndex].label || '';

            if (label) {
              label += ': ';
            }

            return label + result;
          }
        }
      },
      responsive: false,
      maintainAspectRatio: false,
      scales: {
        xAxes: [{
          stacked: true
        }],
        yAxes: [{
          stacked: true,
          scaleLabel: {
            display: true,
            labelString: 'Hours'
          }
        }]
      }
    }
  });
};

window.roundToQuarterHour = function (duration) {
  var totalTime = duration.toString().split('.');
  var hours = parseInt(totalTime[0]);
  var minutes = Math.floor(duration * 60) - Math.floor(hours * 60);

  if (hours === 0) {
    return minutes + 'min';
  }

  if (minutes > 0) {
    return hours + 'hr ' + minutes + 'min';
  }

  return hours + 'hr';
};

$(document).ready(function () {
  var applyBtn = $('.range_inputs > button.applyBtn');
  $(document).on('click', '.ranges li', function () {
    if ($(this).data('range-key') === 'Custom Range') {
      applyBtn.css('display', 'initial');
    } else {
      applyBtn.css('display', 'none');
    }
  });
  applyBtn.css('display', 'none');
});

/***/ }),

/***/ 10:
/*!**********************************************************!*\
  !*** multi ./resources/assets/js/dashboard/dashboard.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/infy-tracker/resources/assets/js/dashboard/dashboard.js */"./resources/assets/js/dashboard/dashboard.js");


/***/ })

/******/ });