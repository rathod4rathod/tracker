/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 15);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/task/task_time_entry.js":
/*!*****************************************************!*\
  !*** ./resources/assets/js/task/task_time_entry.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$('#task_users').select2({
  width: '100%',
  placeholder: 'All',
  minimumResultsForSearch: -1
});
var firstTime = true; // open detail confirmation model

$(document).on('click', '.taskDetails', function (event) {
  var id = $(event.currentTarget).data('id');
  startLoader();
  $('#no-record-info-msg').hide();
  $('#taskDetailsTable').hide();
  $('.time-entry-data').hide();
  firstTime = true;
  $.ajax({
    url: taskUrl + id + '/' + 'users',
    type: 'GET',
    success: function success(result) {
      $('#task_users').empty('');
      $('#task_users').attr('data-task_id', id);
      var newOption = new Option('All', 0, false, false);
      $('#task_users').append(newOption).trigger('change');
      $.each(result, function (key, value) {
        var newOption = new Option(value, key + '-' + id, false, false);
        $('#task_users').append(newOption);
      });
    }
  });
});
$(document).on('change', '#task_users', function () {
  var taskId = $(this).attr('data-task_id');
  var taskUserId = $(this).val().split('-');
  var userId = 0;

  if (taskUserId.length > 1) {
    taskId = taskUserId[1];
    userId = taskUserId[0];
  }

  var url = taskDetailUrl + '/' + taskId;
  var startSymbol = '?';

  if (userId !== 0) {
    startSymbol = '&';
    url = url + '?user_id=' + userId;
  }

  if (reportStartDate != '' && reportEndDate != '') {
    url = url + startSymbol + 'start_time=' + reportStartDate + '&end_time=' + reportEndDate;
  }

  $.ajax({
    url: url,
    type: 'GET',
    success: function success(result) {
      if (result.success) {
        var data = result.data;

        var _url = taskUrl + data.project.prefix + '-' + data.task_number;

        $('#task-heading').html('<h5>Task: <a href=\'' + _url + '\' style=\'color: #0f6683\'>' + data.title + '</a></h5>');
        drawTaskDetailTable(data);
      }
    }
  });
});

window.drawTaskDetailTable = function (data) {
  if (data.totalDuration === 0 && firstTime) {
    $('#no-record-info-msg').show();
    $('.time-entry-data').hide();
    stopLoader();
    return true;
  }

  firstTime = false;
  var taskDetailsTable = $('#taskDetailsTable').DataTable({
    destroy: true,
    paging: true,
    data: data.time_entries,
    searching: false,
    lengthChange: false,
    columns: [{
      className: 'details-control',
      defaultContent: '<a class=\'btn btn-success collapse-icon action-btn btn-sm\'><span class=\'fa fa-plus-circle action-icon\'></span></a>',
      data: null,
      orderable: false
    }, {
      data: 'user.name'
    }, {
      data: 'start_time'
    }, {
      data: 'end_time'
    }, {
      data: function data(row) {
        return roundToQuarterHourAll(row.duration);
      }
    }, {
      orderable: false,
      data: function data(_data) {
        return '<a title=\'Edit\' class=\'btn action-btn btn-primary btn-sm mr-1\' onclick=\'renderTimeEntry(' + _data.id + ')\' ><i class=\'cui-pencil action-icon\'></i></a>' + '<a title=\'Delete\' class=\'btn action-btn btn-danger btn-sm\'  onclick=\'deleteTimeEntry(' + _data.id + ')\'><i class=\'cui-trash action-icon\'></i></a>';
      },
      visible: taskDetailActionColumnIsVisible
    }]
  });
  $('#taskDetailsTable th:first').removeClass('sorting_asc');
  $('.time-entry-data').show();
  $('#taskDetailsTable').show();
  $('#user-drop-down-body').show();
  $('#no-record-info-msg').hide();
  stopLoader();
  $('#taskDetailsTable tbody').off('click', 'tr td.details-control');
  $('#taskDetailsTable tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = taskDetailsTable.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      $(this).children().children().removeClass('fa-minus-circle').addClass('fa-plus-circle');
      row.child.hide();
      tr.removeClass('shown');
    } else {
      // Open this row
      $(this).children().children().removeClass('fa-plus-circle').addClass('fa-minus-circle');
      row.child('<div style="padding-left:50px;">' + nl2br(row.data().note) + '</div>').show();
      tr.addClass('shown');
    }
  });
  $('#taskDetailsTable_wrapper').css('width', '100%');
  $('#total-duration').html('<strong>Total duration: ' + data.totalDuration + ' || ' + data.totalDurationMin + ' Minutes</strong>');
};

/***/ }),

/***/ 15:
/*!***********************************************************!*\
  !*** multi ./resources/assets/js/task/task_time_entry.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/infy-tracker/resources/assets/js/task/task_time_entry.js */"./resources/assets/js/task/task_time_entry.js");


/***/ })

/******/ });