@extends('layouts.app')
@section('title')
    Departments
@endsection

@section('content')
   @livewire('departments')
@endsection

@section('scripts')
    <script>
        let departmentCreateUrl = "{{ route('departments.store') }}";
        let departmentUrl = "{{url('departments')}}/";
    </script>
    <script src="{{ mix('assets/js/department/department.js') }}"></script>
@endsection
