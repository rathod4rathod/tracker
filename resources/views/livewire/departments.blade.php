<div class="bg-red-500">
     <div class="container-fluid">
        <div class="animated fadeIn">
            @include('flash::message')
            <div class="page-header">
                <h3>Departments</h3>
                <div>
                    <a href="#" class="btn btn-primary" wire:click="modelFlagOpen"></i>New Department</a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            @include('departments.table')
                        </div>
                    </div>
                    <div id="AddModal" class="modal fade" role="dialog" tabindex="-1">
					    <div class="modal-dialog">
					        <!-- Modal content-->
					        <div class="modal-content">
					            <div class="modal-header">
					                <h5 class="modal-title">New Department</h5>
					                <button type="button" aria-label="Close" class="close" data-dismiss="modal">×</button>
					            </div>
					            <form id="addDeptForm" wire:submit.prevent="addDepartment">
						            <div class="modal-body">
						                <div class="alert alert-danger" style="display: none" id="validationErrorsBox"></div>
						                <div class="row">
						                    <div class="form-group col-sm-12">
						                        {!! Form::label('name', 'Name') !!}<span class="required">*</span>
						                        <input id="name" class="form-control"  name="name" type="text" value="" wire:modal="name">
					                            @error('name') <span class="error">{{ $message }}</span> @enderror

						                    </div>
						                </div>
						                <div class="text-right">
						                    {!! Form::button('Save', ['type' => 'submit', 'class' => 'btn btn-primary', 'id' => 'btnSave', 'data-loading-text' => "<span class='spinner-border spinner-border-sm'></span> Processing..."]) !!}
						                    <button type="button" id="btnCancel" class="btn btn-light ml-1" data-dismiss="modal">Cancel</button>
						                </div>
						            </div>
					            </form>
					        </div>
					    </div>
					</div>

                    @include('departments.edit_modal')
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script type="text/javascript">
    window.livewire.on('openModel', param => {
    	var m = param['flag'];
    	setTimeout(function(){ $('#AddModal').modal(''+m+''); }, 30);    	
    		 $('#addDeptForm').prev().click();
    	return false;
    });
</script>
@endpush

